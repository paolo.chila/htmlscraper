from abc import ABC, abstractmethod


class HttpRetriever(ABC):
    @abstractmethod
    def retrieve(self, url: str) -> str:
        pass

    @abstractmethod
    def is_reachable(self, url) -> bool:
        pass
