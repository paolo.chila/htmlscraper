from abc import ABC, abstractmethod
from typing import Optional

from scraperapi.core.common import ScrapeResult


class ScrapeResultGateway(ABC):
    @abstractmethod
    def store(self, result: ScrapeResult) -> None:
        pass

    @abstractmethod
    def retrieve(self, url: str) -> Optional[ScrapeResult]:
        pass
