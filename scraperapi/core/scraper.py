import re
from collections import defaultdict
from datetime import datetime, timedelta, timezone
from typing import Dict, List
from urllib.parse import urljoin, urlparse

import attr
from bs4 import BeautifulSoup, Doctype

from scraperapi.core.common import ScrapeResult
from scraperapi.core.persistence import ScrapeResultGateway
from scraperapi.core.retriever import HttpRetriever

HEADINGS_REGEX = re.compile("^h[1-6]")
CACHE_DURATION = timedelta(hours=24)


@attr.s(auto_attribs=True, kw_only=True)
class Link:
    url: str
    external: bool
    reachable: bool


def is_login_form(tag) -> bool:
    result = tag.name == "form" and tag.find("input", type="password") is not None
    return result


@attr.s(auto_attribs=True, init=False)
class HtmlAccessor:
    _soup: BeautifulSoup

    def __init__(self, html: str) -> None:
        self._soup = BeautifulSoup(html, "html.parser")

    def title(self):
        return self._soup.title.string if self._soup.title else None

    def html_version(self):
        for element in self._soup.contents:
            if isinstance(element, Doctype):
                return element.string
        return "<unknown>"

    def headings(self) -> Dict[str, int]:
        heading_summary: Dict[str, int] = defaultdict(int)
        for heading in self._soup.find_all(HEADINGS_REGEX):
            heading_summary[heading.name] += 1
        return heading_summary

    def links(self) -> List[str]:
        return [
            link["href"] for link in self._soup.find_all("a") if link.has_attr("href")
        ]

    def login_forms(self) -> int:
        forms = self._soup.find_all(is_login_form)
        return len(forms)


@attr.s(auto_attribs=True, kw_only=True)
class HtmlStatScraper:
    retriever: HttpRetriever
    persister: ScrapeResultGateway

    def analyse(self, url: str) -> ScrapeResult:

        cached_result = self.persister.retrieve(url=url)

        if cached_result and (
            datetime.now(timezone.utc) - cached_result.stored < CACHE_DURATION
        ):
            return cached_result

        html_accessor = HtmlAccessor(self.retriever.retrieve(url))

        links = [
            Link(
                url=href,
                external=self.is_external(source_url=url, destination_url=href),
                reachable=self.is_reachable(source_url=url, destination_url=href),
            )
            for href in html_accessor.links()
        ]

        number_of_external_links = len([l for l in links if l.external])

        number_of_internal_links = len(links) - number_of_external_links

        number_of_unreachable_links = len([l for l in links if not l.reachable])

        result = ScrapeResult(
            url=url,
            html_version=html_accessor.html_version(),
            title=html_accessor.title(),
            headings=html_accessor.headings(),
            login_forms=html_accessor.login_forms(),
            external_links=number_of_external_links,
            internal_links=number_of_internal_links,
            unreachable_links=number_of_unreachable_links,
        )

        self.persister.store(result)
        return result

    def is_external(self, source_url: str, destination_url: str) -> bool:
        parsed_src = urlparse(source_url)
        parsed_dst = urlparse(destination_url)
        return (
            parsed_src.hostname != parsed_dst.hostname if parsed_dst.hostname else False
        )

    def is_reachable(self, source_url: str, destination_url: str) -> bool:
        normalized_url: str = destination_url
        parsed_dst = urlparse(destination_url)
        if parsed_dst.hostname is None:
            normalized_url = urljoin(source_url, destination_url)
        return self.retriever.is_reachable(normalized_url)
