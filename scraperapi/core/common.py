from datetime import datetime, timezone
from typing import Dict, Optional

import attr


@attr.s(auto_attribs=True, kw_only=True)
class ScrapeResult:
    url: str
    html_version: str
    title: Optional[str] = None
    headings: Dict[str, int] = {}
    internal_links: int = 0
    external_links: int = 0
    unreachable_links: int = 0
    login_forms: int = 0
    stored: datetime = datetime.now(timezone.utc)

    def as_dict(self) -> Dict:
        return attr.asdict(self)
