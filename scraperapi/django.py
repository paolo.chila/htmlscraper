from typing import Optional

from scraperapi.core.common import ScrapeResult
from scraperapi.models import PersistedScrapeResult


class DjangoScrapeResultGateway:
    def store(self, result: ScrapeResult) -> None:
        persisted = PersistedScrapeResult(
            url=result.url,
            html_version=result.html_version,
            title=result.title,
            internal_links=result.internal_links,
            external_links=result.external_links,
            unreachable_links=result.unreachable_links,
            login_forms=result.login_forms,
            h1=result.headings.get("h1", 0),
            h2=result.headings.get("h2", 0),
            h3=result.headings.get("h3", 0),
            h4=result.headings.get("h4", 0),
            h5=result.headings.get("h5", 0),
            h6=result.headings.get("h6", 0),
        )

        persisted.save()

    def retrieve(self, url: str) -> Optional[ScrapeResult]:
        try:
            persisted = PersistedScrapeResult.objects.get(pk=url)
            return ScrapeResult(
                url=persisted.url,
                html_version=persisted.html_version,
                title=persisted.title,
                internal_links=persisted.internal_links,
                external_links=persisted.external_links,
                unreachable_links=persisted.unreachable_links,
                login_forms=persisted.login_forms,
                stored=persisted.stored,
                headings={
                    heading: getattr(persisted, heading)
                    for heading in ["h1", "h2", "h3", "h4", "h5", "h6"]
                    if getattr(persisted, heading) > 0
                },
            )
        except PersistedScrapeResult.DoesNotExist:
            return None
