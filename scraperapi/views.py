from django.http import JsonResponse

from scraperapi.core.scraper import HtmlStatScraper
from scraperapi.django import DjangoScrapeResultGateway
from scraperapi.requests_retriever import RequestsRetriever


def index(request):
    target_url: str = request.GET["url"]

    result = HtmlStatScraper(
        retriever=RequestsRetriever(), persister=DjangoScrapeResultGateway()
    ).analyse(target_url)

    return JsonResponse(result.as_dict())
