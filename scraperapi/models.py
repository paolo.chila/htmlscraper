from django.db import models


class PersistedScrapeResult(models.Model):
    url = models.URLField(max_length=500, primary_key=True)
    html_version = models.CharField(max_length=200)
    title = models.CharField(max_length=200, null=True)
    h1 = models.PositiveSmallIntegerField(default=0)
    h2 = models.PositiveSmallIntegerField(default=0)
    h3 = models.PositiveSmallIntegerField(default=0)
    h4 = models.PositiveSmallIntegerField(default=0)
    h5 = models.PositiveSmallIntegerField(default=0)
    h6 = models.PositiveSmallIntegerField(default=0)
    internal_links = models.PositiveSmallIntegerField(default=0)
    external_links = models.PositiveSmallIntegerField(default=0)
    unreachable_links = models.PositiveSmallIntegerField(default=0)
    login_forms = models.PositiveSmallIntegerField(default=0)
    stored = models.DateTimeField(auto_now_add=True)
