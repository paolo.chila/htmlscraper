from unittest import TestCase
from unittest.mock import Mock, ANY

from scraperapi.core.persistence import ScrapeResultGateway
from scraperapi.core.retriever import HttpRetriever
from scraperapi.core.scraper import HtmlStatScraper, ScrapeResult


class TestHtmlStatScraper(TestCase):
    def setUp(self):
        self.mock_persister = Mock(spec=ScrapeResultGateway)
        self.mock_retriever = Mock(spec=HttpRetriever)

        self.scraper = HtmlStatScraper(
            retriever=self.mock_retriever, persister=self.mock_persister
        )

    def test_scrape_empty_doc(self):
        self.mock_persister.retrieve.return_value = None
        self.mock_retriever.retrieve.return_value = """
            <!DOCTYPE html>
            <html lang="en">
            </html>
        """

        self.assertEqual(
            self.scraper.analyse("somefunkyurl"),
            ScrapeResult(
                url="somefunkyurl", title=None, html_version="html", stored=ANY
            ),
        )

    def test_scrape_empty_doc_lengthy_doctype(self):

        self.mock_persister.retrieve.return_value = None
        self.mock_retriever.retrieve.return_value = """
            <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
            <html lang="en">
            </html>
        """

        self.assertEqual(
            self.scraper.analyse("somefunkyurl"),
            ScrapeResult(
                url="somefunkyurl",
                title=None,
                html_version='HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" '
                '"http://www.w3.org/TR/html4/strict.dtd"',
                stored=ANY,
            ),
        )

    def test_scrape_doc_title(self):
        self.mock_persister.retrieve.return_value = None

        self.mock_retriever.retrieve.return_value = """
            <!DOCTYPE html>
            <html lang="en">
              <head>
                <meta charset="utf-8">
                 <title>title</title>
              </head>
              <body>
              </body>
            </html>
        """

        self.assertEqual(
            self.scraper.analyse("somefunkyurl"),
            ScrapeResult(
                url="somefunkyurl", title="title", html_version="html", stored=ANY,
            ),
        )

    def test_scrape_headings(self):
        self.mock_persister.retrieve.return_value = None

        self.mock_retriever.retrieve.return_value = """
            <!DOCTYPE html>
            <html lang="en">
              <head>
                <meta charset="utf-8">
                 <title>title</title>
              </head>
              <body>
              <h1>Heading 1</h1>
              <H1>Heading 1 Caps</H1>
              <h1>Heading 1 bis </h1>
              <h2>Heading 2</h2>
              <h6>Heading 6</h6>
              </body>
            </html>
        """

        self.assertEqual(
            self.scraper.analyse("somefunkyurl"),
            ScrapeResult(
                url="somefunkyurl",
                title="title",
                html_version="html",
                headings={"h1": 3, "h2": 1, "h6": 1},
                stored=ANY,
            ),
        )

    def test_scrape_login_forms(self):
        self.mock_persister.retrieve.return_value = None

        self.mock_retriever.retrieve.return_value = """
        <!DOCTYPE html>
        <html lang="en">
          <head>
            <meta charset="utf-8">
             <title>title</title>
          </head>
          <body>
          <form action="action_page.php" method="post">
            <input type="text" placeholder="Enter Username" name="uname" required>
            <input type="password" placeholder="Enter Password" name="psw" required>
            <button type="submit">Login</button>
          </form> 
          <form >
          </body>
        </html>
        """

        self.assertEqual(
            self.scraper.analyse("somefunkyurl"),
            ScrapeResult(
                url="somefunkyurl",
                title="title",
                html_version="html",
                login_forms=1,
                stored=ANY,
            ),
        )

    def test_scrape_simple_internal_link(self):
        self.mock_persister.retrieve.return_value = None
        self.mock_retriever.is_reachable.return_value = True

        self.mock_retriever.retrieve.return_value = """
            <!DOCTYPE html>
            <html lang="en">
              <head>
                <meta charset="utf-8">
                 <title>title</title>
              </head>
              <body>
                <a href="https://somesite/someresource"> internal link </a>
              </body>
            </html>
        """

        actual = self.scraper.analyse("https://somesite/someresource")
        expected = ScrapeResult(
            url="https://somesite/someresource",
            title="title",
            html_version="html",
            internal_links=1,
            external_links=0,
            unreachable_links=0,
            stored=ANY,
        )
        self.assertEqual(actual, expected)

    def test_scrape_simple_relative_link(self):
        self.mock_persister.retrieve.return_value = None
        self.mock_retriever.is_reachable.return_value = True

        self.mock_retriever.retrieve.return_value = """
            <!DOCTYPE html>
            <html lang="en">
              <head>
                <meta charset="utf-8">
                 <title>title</title>
              </head>
              <body>
                <a href="somerelativelocalresource"> relative local link </a>
              </body>
            </html>
        """

        actual = self.scraper.analyse("https://somesite/someresource")
        expected = ScrapeResult(
            url="https://somesite/someresource",
            title="title",
            html_version="html",
            internal_links=1,
            external_links=0,
            unreachable_links=0,
            stored=ANY,
        )
        self.assertEqual(actual, expected)

    def test_scrape_multiplemixedlinks(self):
        self.mock_persister.retrieve.return_value = None
        self.mock_retriever.is_reachable.return_value = True

        self.mock_retriever.retrieve.return_value = """
            <!DOCTYPE html>
            <html lang="en">
              <head>
                <meta charset="utf-8">
                 <title>title</title>
              </head>
              <body>
                <a href="https://somesite/someresource"> internal link </a>
                <a href="https://someothersite/someotherresource"> external
                link </a>
                <a href="/somelocalresource"> absolute local link </a>
                <a href="somerelativelocalresource"> relative local link </a>
              </body>
            </html>
        """
        self.assertEqual(
            self.scraper.analyse("https://somesite/someresource"),
            ScrapeResult(
                url="https://somesite/someresource",
                title="title",
                html_version="html",
                internal_links=3,
                external_links=1,
                unreachable_links=0,
                stored=ANY,
            ),
        )

    def test_scrape_links_reachable_unreachable(self):
        self.mock_persister.retrieve.return_value = None

        self.mock_retriever.retrieve.return_value = """
            <!DOCTYPE html>
            <html lang="en">
              <head>
                <meta charset="utf-8">
                 <title>title</title>
              </head>
              <body>
                <a href="https://somesite/inaccessibleresource"> broken link </a>
                <a href="https://someothersite/someotherresource"> external
                link </a>
              </body>
            </html>
        """

        def is_reachable(url):
            return "inaccessible" not in url

        self.mock_retriever.is_reachable.side_effect = is_reachable

        self.assertEqual(
            self.scraper.analyse("https://somesite/someresource"),
            ScrapeResult(
                url="https://somesite/someresource",
                title="title",
                html_version="html",
                internal_links=1,
                external_links=1,
                unreachable_links=1,
                stored=ANY,
            ),
        )
