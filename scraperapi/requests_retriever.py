import requests

from scraperapi.core.retriever import HttpRetriever


class RequestsRetriever(HttpRetriever):
    def retrieve(self, url: str) -> str:
        response = requests.get(url)
        return response.text

    def is_reachable(self, url: str) -> bool:
        try:
            response = requests.head(url, allow_redirects=True)
            response.raise_for_status()
            return True
        except:  # noqa: E722
            return False
