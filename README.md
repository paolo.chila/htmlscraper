## Analyzing websites

### The task

The objective is to build an API service that does some analysis of a web-page/URL.

## Functional Requirements

- The application should accept the URL of the webpage being analyzed.

- After submitting the URL to the server, trigger the server-side analysis process.

- After processing the results should be shown returned to the user. The response comprises the following information:

  - What HTML version has the document?

  - What is the page title?

  - How many headings of what level are in the document?

  - How many internal and external links are in the document? Are there any inaccessible links and how many?

  - Did the page contain a login-form?

In case the requested URL is not reachable, an error message should be returned. The message should contain the HTTP status-code and some useful error description.

## Non-Functional Requirements:

The backend should cache the scraping results for each URL for 24 hours such that your backend does not have to redo the scraping for any given URL within the next 24 hours.
Your application server as well as your data store should be run in separate Docker containers such that they can later on be scaled independently.

## Notes on the solution


### How to run the application

In order to run the application is enough to run the command

`docker-compose up --build --force-recreate`

and then perform a request on `localhost:8000`, for example:

`curl -Lv http://localhost:8000/scraperapi/?url=https://nextmatter.com/`

### Dependencies

The solution uses:
- django as a web and persistence framework
- requests as http client library
- urllib for URL manipulation
- beautifulsoup4 for html parsing

The main class is HtmlStatScraper in the scraperapi.core module which
orchestrates retrieval of cached results, creation and persistence of new ones.

### Basic algorithm

The HtmlStatScraper class will check if there's already a valid stored result
for the requested url: in such a case the data coming from the db is
immediately returned.

If there's no cached result available, it will use a retriever to perform an
HTTP GET using the provided URL, pass the html content to an accessor which
will look for the relevant stats.

The strategy for recognizing some of the requested elements is the following:
- links: we are looking for `<a>` elements with a non-empty href
- login forms: we are looking for `<form>` containing a `<input type=password>`
- html version: we look for the content of `DOCTYPE` string, returning
"unknown" if we don't find any

Once all the relevant elements are extracted from html we perform some
postprocessing on all the links to determine:
- if they are internal or external: we check if the hostname of the url matches
with the one of the page being analyzed
- if they are reachable: we perform a HTTP HEAD to check that we can get a
correct answer fpr the URL

Finally the result is stored on db for caching purposes.


### Implementation notes

The solution has been split into a core package containing the main logic of
the application and adapters that take care of interacting with external
systems (db or other websites) so that testing and switching out different
implementations for storage or fetching content would be easier.

The docker-compose provided allows for easy development and testing since the
code of the application is mounted as volume and django test server reloads
correctly as soon as modifications happen. The port 8000 is mapped on the
docker host to allow access through localhost. A volume attached to the
database container will persist the db data across multiple runs.

As for the database model a simplified schema has been adopted by denormalizing
the relationship between heading count and the report itself: there are 6
columns h1 to h6 that hold the counts for the respective headings level. If we
were to bring the model to normal form there should be a foreign key on a
headings table that links the count to a specific report.

In the current solution all the fetching of http resources is synchronous and
serialized, it would be beneficial from a performance standpoint to switch to
asyncio (using aio-http for example) so that all the http requests can be run
concurrently.

There are still some tests missing, mostly django tests to verify the
application behaviour when called via HTTP and the behaviour when storage is
involved.
